#include "mm.h"
#include <stdio.h>

/* Prints time cost for mm doing
 * a series of get() and put(). 
 * adapted from provided timing function.*/
void time_mm()
{
    int i;
    struct timeval time_1, time_2;
    void** a = (void**)malloc(sizeof(void*)*NUMCHUNKS);
    if (a == NULL) {
        fprintf(stderr, "Failed malloc a\n");
        exit(1);
    }
    mm_t mm_test;

    if (mm_init(&mm_test, NUMCHUNKS, CHUNKSIZE) == -1) {
        fprintf(stderr, "Failed mm_init()\n");
        exit(1);
    }
    gettimeofday(&time_1, NULL);

    for (i = 0; i < NUMCHUNKS; i++) {
        a[i] = mm_get(&mm_test);
    }
    for (i = 0; i < NUMCHUNKS; i++) {
        mm_put(&mm_test, a[i]);
    }

    gettimeofday(&time_2, NULL);
    mm_release(&mm_test);

    fprintf(stderr, "%d chunks - mm: %f ms\n", NUMCHUNKS, comp_time(time_1, time_2)/1000.0);
}


int main() {
    time_mm();
    return 0;
}
