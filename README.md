How to compile 
---
```sh
 $ make
```

How to use
---
####To time the native system call
```sh
$ make run_malloc
```

####To time the memory manager (mm) -
```sh
$ make run_mm
```

####To run the packet program -
```sh
$ make run_packet
```

What exactly your program does
---
- Memory Manager : 
> It pre-allocates a pool of memory with a fixed numberof chunks when init() is called.  When a get() is requested, a pointer to the beginning of a free chunks is returned (similar to malloc() in c). When a put() is requested, a chunk is put back (marked as FREE). Finally when release() is called, memory allocated for MM is put back to the OS.
		
- Packet Program :
> It's to mimic the server behavior using mm and signal. A series of actions including get_packet() and copying it to mm is enclosed in a signal handler that invoked when a timer counts to zero. The handler also blocks all the signal when executed so it is singal-handler-safe. When all packets arrive, they are assembled into a char array and printed out.
