#include "mm.h"
#include <stdio.h>

/* Prints time cost for native sys call doing
 * a series of malloc() and free().
 * adapted from provided timing function. */
void time_native()
{
    int i;
    struct timeval time_1, time_2;
    void** a = (void**)malloc(sizeof(void*)*NUMCHUNKS);
    if  (a == NULL) {
        fprintf(stderr, "Failed malloc a\n");
        exit(1);
    }

    gettimeofday(&time_1, NULL);

    for (i = 0; i < NUMCHUNKS; i++) {
        a[i] = (void*)malloc(CHUNKSIZE);
    }
    
    for(i = 0; i < NUMCHUNKS; i++) {
        free(a[i]);
    }
    gettimeofday(&time_2, NULL);
    fprintf(stderr, "%d chunks - native: %f ms\n", NUMCHUNKS, comp_time(time_1, time_2)/1000.0);
    free(*a);
    free(a);
}

int main()
{
    time_native();
    return 0;
}
