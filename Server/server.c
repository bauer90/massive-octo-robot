#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include "util.h"
#define MAX_THREADS 100
#define MAX_QUEUE_SIZE 100
#define MAX_REQUEST_LENGTH 1024

//Structure for queue.
typedef struct request_queue{
    int m_socket;
    char m_szRequest[MAX_REQUEST_LENGTH];
} request_queue_t;

/* a structure for a queue, followed by its 
 * operations. */
typedef struct array_queue {
    request_queue_t* queue;
    int length;
    int num_occupied;
    int index_head;
    int index_tail;
} array_queue_t;

int is_full_queue(const array_queue_t* const src)
{
    if (src->length <= src->num_occupied) {
        return 1;
    } else {
        return 0;
    }
}

request_queue_t dequeue(array_queue_t* const q)
{
    if (q->num_occupied <= 0) {
        fprintf(stderr, "empty queue.\n");
        return;
    }
    int old_head = q->index_head;
    q->index_head = (q->index_head + 1) % (q->length);
    q->num_occupied--;
    return *((q->queue) + old_head);
}

int enqueue(array_queue_t* const q, const request_queue_t* const entry)
{
    if (is_full_queue(q)) {
        fprintf(stderr, "full queue.\n");
        return -1;
    }
    ((q->queue)[q->index_tail]).m_socket = entry->m_socket;
    strcpy(((q->queue)[q->index_tail]).m_szRequest, entry->m_szRequest);
    q->index_tail = (q->index_tail + 1) % (q->length);
    q->num_occupied++;
    return 0;
}

char* trim_slash(char* src)
{
    if (src == NULL) {
        fprintf(stderr, "src == NULL in trim_slash()\n");
        return NULL;
    } else if (src[0] == '/' && strlen(src) > 1) {
        return src+1;
    } else {
        return src;
    }
}

/* return "text/plain" by default if no match */
char* filetype(char* src)
{
    size_t length;
    if (src == NULL) {
        fprintf(stderr, "src == NULL in filetype().\n");
        return NULL;
    }
    length = strlen(src);
    if (length >= 5 && !strcmp(&src[length-5], ".html")) {
        return "text/html";
    } else if (length >= 4 && !strcmp(&src[length-4], ".txt")) {
        return "text/plain";
    } else if (length >= 4 && !strcmp(&src[length-4], ".gif")) {
        return "image/gif";
    } else if (length >= 4 && !strcmp(&src[length-4], ".jpg")) {
        return "image/jpeg";
    } else {
        return "text/plain";
    }
}

/* queue length, queue and ptr to log as global variables */
int qlen;
array_queue_t req_queue;
FILE* fp_log;

/* declare mutex and condvar as globle variables */
pthread_mutex_t mtx = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mtx_log = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond_can_enqueue = PTHREAD_COND_INITIALIZER;/* this will be signaled when any dequeue occurs.*/
pthread_cond_t cond_can_dequeue = PTHREAD_COND_INITIALIZER;/* this will be signaled when any enqueue occurs */

/* dispatch thread function. 
 * Gets requests and stores them in a global queue. */
void * dispatch(void * arg)
{
    int tid_this = *((int*)arg), fildes, s;
    request_queue_t queue_entry;
    char* filename = calloc(MAX_REQUEST_LENGTH, sizeof(char));
    if(filename == NULL) {
        fprintf(stderr, "calloc() failed in dispatch %d\n", tid_this);
        exit(1);
    }
    /* in a loop, call accept_connection() and get_request()
     *  and then store the request in a queue. */
    for(;;) {
        /* try to get a request. If no request, then try again in
         * the next loop */
        fildes = accept_connection();
        if (fildes < 0) {
            fprintf(stderr, "In dispatch No.%d,\n accept_connection() returns negative number", tid_this);
            pthread_exit(NULL);
        }
        s = get_request(fildes, filename);
        if (s != 0) {
            fprintf(stderr, "In dispatch No.%d,\n get_request() returns nonzero number", tid_this);
            continue;
        }
        /* load request into a temporary queue entry */
        queue_entry.m_socket = fildes;
        strcpy(queue_entry.m_szRequest, trim_slash(filename));
        /* Start - QUEUE Critical Section */
        s = pthread_mutex_lock(&mtx);
        if (s != 0) {
            fprintf(stderr, "lock(&mtx) failed in dispatcher thread %d.\n", tid_this);
            exit(1);
        }
        /* try to enqueue the request. */
        while (is_full_queue(&req_queue)) {
            s = pthread_cond_wait(&cond_can_enqueue, &mtx);
            if (s != 0) {
                fprintf(stderr, "cond_wait(can_enqueue) failed in dispatcher %d\n", tid_this);
                exit(1);
            }
        }
        s = enqueue(&req_queue, &queue_entry);
        if (s != 0) {
            fprintf(stderr, "enqueue() failed in dispatcher thread %d.\n", tid_this);
            exit(1);
        }
        s = pthread_cond_signal(&cond_can_dequeue);
        if (s != 0) {
            fprintf(stderr, "cond_signal(can_dequeue) failed in dispatcher %d.\n", tid_this);
            exit(1);
        }
        s = pthread_mutex_unlock(&mtx);
        if (s != 0) {
            fprintf(stderr, "unlock(&mtx) failed in dispatcher thread %d.\n", tid_this);
            exit(1);
        }
        /* End - QUEUE Critical Section */
    }
    return NULL;
}

void * worker(void * arg)
{
    request_queue_t req_entry;
    struct stat st_file;
    void* file_content;
    char* string_error;
    int tid_this = *((int*)arg), counter = 0, s;
    FILE* fp_requested;
    off_t filesize;
    for(;;) {
        /* Start - QUEUE Critical Section */
        s = pthread_mutex_lock(&mtx);
        if (s != 0) {
            fprintf(stderr, "lock(mtx) failed in worker %d.\n", tid_this);
            exit(1);
        }
        /* try to retrieve request from req_queue into a temporary
         * queue entry */
        while (req_queue.num_occupied <= 0) {
            s = pthread_cond_wait(&cond_can_dequeue, &mtx);
            if (s != 0) {
                fprintf(stderr, "cond_wait(can_dequeue) failed in worker %d\n", tid_this);
                exit(1);
            }
        }
        req_entry = dequeue(&req_queue);
        s = pthread_cond_signal(&cond_can_enqueue);
        if (s != 0) {
            fprintf(stderr, "cond_signal(can_enqueue) failed in worker %d\n", tid_this);
            exit(1);
        }
        s = pthread_mutex_unlock(&mtx);
        if (s != 0) {
            fprintf(stderr, "unlock(mtx) failed in worker %d.\n", tid_this);
            exit(1);
        }
        /* End - QUEUE Critical Section */
        counter++;
        /* now we have the filename. open the file and read
         * into a segment of memory */
        /* first try to open this file */
        fp_requested = fopen(req_entry.m_szRequest, "rb");
        if (fp_requested == NULL) { /* failed opening file */
            string_error = strerror(errno);
            /* failed openning the file. call return_error() */
            s = return_error(req_entry.m_socket, string_error);
            if (s != 0) {
                fprintf(stderr, "In worker %d,\n return_error() failed.\n", tid_this);
                exit(1);
            }
            /* and write to log. Start - LOG Critical Section */
            s = pthread_mutex_lock(&mtx_log);
            if (s != 0) {
                fprintf(stderr, "In worker %d,\n lock(mtx_log) failed at fail", tid_this);
                exit(1);
            }
            fprintf(fp_log, "[%d][%d][%d][%s][%s][%d][%s]\n", tid_this, counter, req_entry.m_socket, req_entry.m_szRequest, string_error, 0, "MISS");
            fflush(fp_log);
            s = pthread_mutex_unlock(&mtx_log);
            if (s != 0) {
                fprintf(stderr, "In worker %d,\n unlock(mtx_log) failed at fail\n", tid_this);
                exit(1);
            }
            /* End - LOG Critical Section */
        } else { /* succeeded opening file */
            /* check the size of the file */
            s = stat(req_entry.m_szRequest, &st_file);
            if (s != 0) {
                fprintf(stderr, "In worker %d,\n stat() failed.\n", tid_this);
                exit(1);
            }
            filesize = st_file.st_size;
            /* malloc a segment of memory to hold file content */
            file_content = malloc(filesize);
            if (file_content == NULL) {
                fprintf(stderr, "In worker %d,\n malloc(filesize) failed.\n", tid_this);
                exit(1);
            }
            /* read content into memory */
            s = fread(file_content, 1, filesize, fp_requested);
            if (s != filesize) {
                fprintf(stderr, "In worker %d,\n fread() failed.\n", tid_this);
                exit(1);
            }
            /* finally call return_result() and close file */
            s = return_result(req_entry.m_socket, filetype(req_entry.m_szRequest), file_content, filesize);
            if (s != 0) {
                fprintf(stderr, "In worker %d,\n return_result() failed.\n", tid_this);
                exit(1);
            }
            s = fclose(fp_requested);
            if (s != 0) {
                fprintf(stderr, "In worker %d,\n fclose(fp_requested) failed.\n", tid_this);
                exit(1);
            }
            /* write to log. Start - LOG Critical Section */
            s = pthread_mutex_lock(&mtx_log);
            if (s != 0) {
                fprintf(stderr, "In worker %d,\n lock(mtx_log) failed at success", tid_this);
                exit(1);
            }
            fprintf(fp_log, "[%d][%d][%d][%s][%d][%d][%s]\n", tid_this, counter, req_entry.m_socket, req_entry.m_szRequest, (int)filesize, 0, "MISS");
            fflush(fp_log);
            s = pthread_mutex_unlock(&mtx_log);
            if (s != 0) {
                fprintf(stderr, "In worker %d,\n unlock(mtx_log) failed at success\n", tid_this);
                exit(1);
            }
            /* End - LOG Critical Section */
        }
    }
    return NULL;
}

int main(int argc, char **argv)
{
    int i, s, port, num_dispatchers, num_workers, cache_entries;
    char* path;
  
    //Error check first.
    if(argc != 6 && argc != 7) {
        printf("usage: %s port path num_dispatcher num_workers queue_length [cache_size]\n", argv[0]);
        return -1;
    }
  
    /* assigning config param and checking error */
    port = atoi(argv[1]);
    if (port < 1025 || port > 65535) {
        fprintf(stderr, "port out of range.\n");
        exit(1);
    }
    path = argv[2];
    num_dispatchers = atoi(argv[3]);
    if (num_dispatchers < 1 || num_dispatchers > MAX_THREADS) {
        fprintf(stderr, "num_dispatchers out of range.\n");
        exit(1);
    }
    num_workers = atoi(argv[4]);
    if (num_workers < 1 || num_workers > MAX_THREADS) {
        fprintf(stderr, "num_workers out of range.\n");
        exit(1);
    }
    qlen = atoi(argv[5]);
    if (qlen < 1 || qlen > MAX_QUEUE_SIZE) {
        fprintf(stderr, "qlen out of range.\n");
        exit(1);
    }
    cache_entries = atoi(argv[6]);
    if (cache_entries < 1 || cache_entries > 100) {
        fprintf(stderr, "cache_entries out of range.\n");
        exit(1);
    }

    /* create the log file */
    fp_log = fopen("web_server_log", "a");
    if (s != 0) {
        fprintf(stderr, "In main(),\n fopen(log) failed.");
        exit(1);
    }

    /* cd to path */
    s = chdir(path);
    if (s != 0) {
        fprintf(stderr, "chdir() failed.\n");
        exit(1);
    }

    /* assigning the global queue with initial values */
    req_queue.queue = calloc(qlen, sizeof(request_queue_t));
    if (req_queue.queue == NULL) {
        fprintf(stderr, "calloc(qlen, sizeof(request_queue_t)) failed.\n");
        exit(1);
    }
    req_queue.length = qlen;
    req_queue.num_occupied = 0;
    req_queue.index_head = 0;
    req_queue.index_tail = 0;

    /* call init() once at the beginning */
    init(port);

    /* declare dispatcher and worker threads */
    pthread_t* dispatcher_tid = calloc(num_dispatchers, sizeof(pthread_t));
    pthread_t* worker_tid = calloc(num_workers, sizeof(pthread_t));

    /* Thread args are passed by reference, so create
     * a separate variable for each pthread index. */
    int* index_array_dispatchers = malloc(sizeof(int)*num_dispatchers);
    for (i = 0; i < num_dispatchers; i++) {
        index_array_dispatchers[i] = i;
    }
    int* index_array_workers = malloc(sizeof(int)*num_workers);
    for (i = 0; i < num_workers; i++) {
        index_array_workers[i] = i;
    }

    /* Initiating dispatchers and workers. */
    for (i = 0; i < num_dispatchers; i++) {
        s = pthread_create(dispatcher_tid+i, NULL, dispatch, (void*)(&(index_array_dispatchers[i])));
        if (s != 0) {
            fprintf(stderr, "Failed creating dispatcher thread No.%d.\n",  i);
            exit(1);
        }
    }
    for (i = 0; i < num_workers; i++) {
        s = pthread_create(worker_tid+i, NULL, worker, (void*)(&(index_array_workers[i])));
        if (s != 0) {
            fprintf(stderr, "Failed creating worker thread No.%d.\n", i);
            exit(1);
        }
    }

    /* Waiting for dispatchers and workers to return */
    for (i = 0; i < num_dispatchers; i++) {
        s = pthread_join(dispatcher_tid[i], NULL);
        if (s != 0) {
            fprintf(stderr, "dispatcher No.%d failed returning.\n", i);
            exit(1);
        }
        fprintf(stderr, "dispatch %d returned.\n", i);
    }
    for (i = 0; i < num_workers; i++) {
        s = pthread_join(worker_tid[i], NULL);
        if (s != 0) {
            fprintf(stderr, "worker No.%d failed returning.\n", i);
            exit(1);
        }
        fprintf(stderr, "worker %d returned.\n", i);
    }

  /* close log file */
    fflush(fp_log);
    s = fclose(fp_log);
    if (s != 0) {
        fprintf(stderr, "In main(),\n fclose(log) failed.\n");
        exit(1);
    }
    return 0;
}