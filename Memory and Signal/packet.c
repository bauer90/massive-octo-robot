#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include "mm.h"

#define MAX_PACKETS 10

typedef char data_t[56];

typedef struct {
    int how_many; /* number of packets in the message */
    int which;    /* which packet in the message -- currently ignored */
    data_t data;  /* packet data */
} packet_t;

/* Keeps track of packets that have arrived for the message */
typedef struct {
    int num_packets;
    void *data[MAX_PACKETS];
} message_t;

static message_t message;   /* current message structure */
static mm_t mm;             /* memory manager will allocate memory for packets */
static int pkt_cnt = 0;     /* how many packets have arrived for current message */
static int pkt_total = 1;   /* how many packets to be received for the message */
static int NUM_MESSAGES = 3; /* number of messages we will receive */



static packet_t get_packet()
{
    static int which = 0;

    packet_t pkt;

    pkt.how_many = 3;
    pkt.which = which;

    switch (which) {
        case 0:
            strcpy(pkt.data, "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
            break;
        case 1:
            strcpy(pkt.data, "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb");
            break;
        default:
            strcpy(pkt.data, "cccccccccccccccccccccccccccccccccccccccccccccccccccccccc");
    }
    which = (which + 1) % 3;
    return pkt;
}

/* In this signal handler- First a chunk of memory is
 * is allocated on mm. Then memcpy is used to copy 
 * packet's data to that chunk. */
static void packet_handler(int sig)
{
    packet_t pkt = get_packet();
    pkt_total = pkt.how_many;
    pkt_cnt++;
    message.num_packets++;

    /* allocates a chunk on mm; aborts if all chunks taken.*/
    void* ptr = mm_get(&mm);
    if (ptr == NULL) {
        fprintf(stderr, "In packet_handler(),\n mm_get() returns NULL. Very likely all chunks taken.\n");
        exit(1);
    }

    /* relates packet.which to chunk's position, so that
     * we know which chunk holds the first packet, which
     * chunk holds the second packet, etc. */
    int packet_index = pkt.which;
    memcpy(ptr, pkt.data, 56);
    message.data[packet_index] = ptr;
}

/*
 * TODO - Create message from packets ... deallocate packets.
 * Return a pointer to the message on success, or NULL and set errno on
 * failure.
 */
static char *assemble_message()
{
    int i;
    int packet_length = 56;

    /* +1 for '\0' character */
    int message_length = packet_length * (message.num_packets) + 1;
    char* msg = (char*)malloc(sizeof(char)*message_length);
    if (msg == NULL) {
        fprintf(stderr, "In assemble_message(),\n malloc() failed.\n");
        exit(1);
    }

    /* memcpy chunk's data to msg. One packet each time. */
    for (i = 0; i < message.num_packets; i++) {
        memcpy(msg+packet_length*i, message.data[i], packet_length);
    }

    /* mark msg's end with \0 */
    *(msg + message_length - 1) = '\0';
  
    /* release memory on mm. (not freeing) */
    for (i = 0; i < message.num_packets; i++) {
        mm_put(&mm, message.data[i]);
    }

    /* reset these for next message */
    pkt_total = 1;
    pkt_cnt = 0;
    message.num_packets = 0;
  
    return msg;
}

int main(int argc, char **argv)
{
    int i;
    char *msg;

    /* init the mm. number of chunks = MAX_PACKETS and
     * each chunk holds 56 char. */
    mm_init(&mm, MAX_PACKETS, 56);

    /* generates a full signal set */
    sigset_t all_signals;
    sigemptyset(&all_signals);
    sigfillset(&all_signals);

    /* use the full signal set as sigaction's sa_mask.
        So packet_handler() will block all signals. */
    struct sigaction blockall_act;
    blockall_act.sa_handler = packet_handler;
    blockall_act.sa_flags = 0;
    blockall_act.sa_mask = all_signals;
    if (sigaction(SIGALRM, &blockall_act, NULL) == -1) {
        fprintf(stderr, "sigaction() failed.\n");
        exit(1);
    }
  
    /* set up and start the timer */
    struct itimerval timer;
    timer.it_value.tv_sec = INTERVAL;
    timer.it_value.tv_usec = INTERVAL_USEC;
    timer.it_interval.tv_sec = INTERVAL;
    timer.it_interval.tv_usec = INTERVAL_USEC;
    if (setitimer(ITIMER_REAL, &timer, NULL) == -1) {
        fprintf(stderr, "setitimer() failed.\n");
        exit(1);
    }

    message.num_packets = 0;

    for (i = 1; i <= NUM_MESSAGES; i++) {
        while (pkt_cnt < pkt_total) {
            pause(); /* block until next packet */
        }

        msg = assemble_message();
    
        if (msg == NULL) {
            perror("Failed to assemble message");
        }
        else {
            fprintf(stderr, "GOT IT: message=%s\n", msg);
            free(msg);
        }
    }

    /* deallocate mm.*/
    mm_release(&mm);
    return EXIT_SUCCESS;
}
