#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include "mm.h"
/* Return usec */
double comp_time(struct timeval time_s, struct timeval time_e)
{
    double elap = 0.0;
    if (time_e.tv_sec > time_s.tv_sec) {
        elap += (time_e.tv_sec - time_s.tv_sec - 1) * 1000000.0;
        elap += time_e.tv_usec + (1000000 - time_s.tv_usec);
    }
    else {
        elap = time_e.tv_usec - time_s.tv_usec;
    }
    return elap;
}


/* init mm with number of chunks = hm and
 * each chunk has sz size. */
int mm_init(mm_t *mm, int hm, int sz)
{
    int i;
    
    /* make sure mm is not NULL */
    if (mm == NULL || hm < 1 || sz < 1) {
        fprintf(stderr, "In mm_init(),\n mm is NULL or size problem exists\n");
        return -1;
    }
  
    /* malloc for all chunks and check error */
    mm->data = (void*)malloc(hm*sz);
    if (mm->data == NULL) {
        fprintf(stderr, "In mm_init(),\n malloc for data failed.\n");
        return -1;
    }
  
    /* assign other properties */
    mm->chunk_size = sz;
    mm->num_chunks = hm;
    mm->cursor = 0;

    /* malloc an "array" of status indicators. Each entry represents a chunk.
    At first they're all FREE */
    mm->status_array = (bool*)malloc(sizeof(bool)*hm);
    if (mm->status_array == NULL) {
        fprintf(stderr, "In mm_init(),\n malloc for status_array failed.\n");
        return -1;
    }
    for (i = 0; i < hm; i++) {
        *((mm->status_array)+i) = FREE;
    }
    return 0;
}

/* returns a free chunk if there exists one;
 * otherwise returns NULL. */
void *mm_get(mm_t *mm)
{
    int i, j;
  
    /* make sure mm is not NULL */
    if (mm == NULL) {
        fprintf(stderr, "In mm_get(),\n mm is NULL");
        return NULL;
    }
  
    /* cursor is where the most possibly FREE chunk is.
     So try it first. */
    j = mm->cursor;
    if (*((mm->status_array)+j) == FREE) {
        *((mm->status_array)+j) = TAKEN;
        if ((mm->cursor) >=  (mm->num_chunks-1)) {
            mm->cursor = 0;
        } else {
            (mm->cursor)++;
        }
        return (mm->data)+(j*(mm->chunk_size));
    }
  
    /* If cursor's chunk is TAKEN, then try from the beginning. */
    for (i = 0; i < mm->num_chunks; i++) {
        if (*((mm->status_array)+i) == FREE) {
            *((mm->status_array) + i) = TAKEN;
            if ((mm->cursor) >= (mm->num_chunks-1)) {
                mm->cursor = 0;
            } else {
                (mm->cursor)++;
            }
            return (mm->data)+(i*(mm->chunk_size));
        }
    }
    /* If still not found, just return NULL. */
    fprintf(stderr, "All chunks taken.\n");
    return NULL;
}

/* put a chunk pointed by arg 'chunk' back to the mm
 * by marking it as 'FREE'. */
void mm_put(mm_t *mm, void *chunk)
{
    int i;
    /* make sure neither mm nor chunk is NULL */
    if (mm == NULL || chunk == NULL) {
        fprintf(stderr, "In mm_put(),\n mm or chunk is NULL\n");
        return;
    }
    /* calculate how far 'chunk' is away from the beginning of 'data';
        divided by chunk_size, then it's the index in status_array */
    i = (chunk-(mm->data)) / (mm->chunk_size);
    mm->cursor = i;
    *((mm->status_array) + i) = FREE;
}

/* Releases the memory allocated for mm's data and 
 * status_array */
void mm_release(mm_t *mm)
{
    if (mm == NULL) {
        fprintf(stderr, "In mm_release(),\n mm is NULL.\n");
        return;
    }
    free(mm->data);
    free(mm->status_array);
}
